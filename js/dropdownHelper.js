function setDropdownCss(dropdown, parent) {
    var css = {
        "background-color": parent.css("background-color"),
        "box-shadow": parent.css("box-shadow")
    };

    dropdown.css(css);
}

function setDropdownPosition(dropdown, parent) {
    var offset = parent[0].offsetTop;
    dropdown.css("top", offset + parseInt(parent.css("height")));
}

$('.dropdown-toggle').on('click', function(e) {
    var toggle = $(this);
    var dropdown = toggle.siblings().eq(0);
    var parent = toggle.parent().eq(0);

    setDropdownCss(dropdown, parent);
    setDropdownPosition(dropdown, parent);
});