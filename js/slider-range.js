$(function() {
    var sliderRange = $("#slider-range");
    var priceFrom = $("#priceFrom");
    var priceTo = $("#priceTo");

    sliderRange.slider({
        range: true,
        min: 500,
        max: 95000,
        values: [priceFrom.val(), priceTo.val()],
        classes: {
            "ui-slider-handle": "polzunok"
        },
        slide: function(event, ui) {
            priceFrom.val(ui.values[0]);
            priceTo.val(ui.values[1]);
        }
    });

    priceFrom.val(sliderRange.slider("values", 0));
    priceTo.val(sliderRange.slider("values", 1));
});
